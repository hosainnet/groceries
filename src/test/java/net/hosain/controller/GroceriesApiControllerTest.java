package net.hosain.controller;

import net.hosain.controller.GroceriesApiController;
import net.hosain.model.Grocery;
import net.hosain.model.GroceryResponse;
import net.hosain.service.ConnectionService;
import net.hosain.service.GroceriesApiService;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GroceriesApiControllerTest {

    GroceriesApiController groceriesApiController;
    private GroceriesApiService mockGroceriesApiService;
    private ConnectionService mockConnectionService;

    @Before
    public void setup() {
        mockGroceriesApiService = mock(GroceriesApiService.class);
        mockConnectionService = mock(ConnectionService.class);
        groceriesApiController = new GroceriesApiController(mockGroceriesApiService, mockConnectionService);
    }

    @Test
    public void testGroceriesListApi() throws IOException {
        ArrayList<Grocery> groceries = new ArrayList<>();
        groceries.add(new Grocery("test1", "10kb", 5.5, "test1"));
        groceries.add(new Grocery("test2", "15kb", 5.5, "test2"));

        Connection mockConnection = mock(Connection.class);
        when(mockConnectionService.getConnection("http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html")).thenReturn(mockConnection);
        Document mockDocument = mock(Document.class);
        Elements mockProducts = mock(Elements.class);

        when(mockConnection.get()).thenReturn(mockDocument);
        when(mockGroceriesApiService.getProducts(mockDocument)).thenReturn(mockProducts);
        when(mockGroceriesApiService.buildGroceriesList(mockProducts)).thenReturn(groceries);

        GroceryResponse groceryResponse = groceriesApiController.list();

        verify(mockConnection).get();
        verify(mockGroceriesApiService).getProducts(mockDocument);
        verify(mockGroceriesApiService).buildGroceriesList(mockProducts);

        assertEquals(groceries, groceryResponse.getResults());
        assertEquals(11, groceryResponse.getTotal(), 0);
    }
}