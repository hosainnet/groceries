package net.hosain.service;

import net.hosain.model.Grocery;
import net.hosain.service.ConnectionService;
import net.hosain.service.GroceriesApiService;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GroceriesApiServiceTest {

    GroceriesApiService groceriesApiService;
    private ConnectionService mockConnectionService;

    @Before
    public void setUp() throws Exception {
        mockConnectionService = mock(ConnectionService.class);
        groceriesApiService = new GroceriesApiService(mockConnectionService);
    }

    @Test
    public void testGetProducts() {
        Document mockDocument = mock(Document.class);

        groceriesApiService.getProducts(mockDocument);

        verify(mockDocument).select(".product");
    }

    @Test
    public void testBuildGroceriesList() throws IOException {
        Element mockProductElement = mock(Element.class);
        Elements mockInnerElement = mock(Elements.class);
        Elements mockPriceElements = mock(Elements.class);

        Connection mockConnection = mock(Connection.class);
        when(mockConnectionService.getConnection("http://localhost")).thenReturn(mockConnection);
        Connection.Response mockConnectionResponse = mock(Connection.Response.class);
        when(mockConnectionResponse.bodyAsBytes()).thenReturn(new byte[]{});
        Document mockDocument = mock(Document.class);
        Element mockDescriptionElement = mock(Element.class);
        when(mockDescriptionElement.text()).thenReturn("test desc");
        when(mockDocument.select(".productText")).thenReturn(new Elements(mockDescriptionElement));
        when(mockConnectionResponse.parse()).thenReturn(mockDocument);
        when(mockConnection.execute()).thenReturn(mockConnectionResponse);

        when(mockProductElement.select(".productInfo h3 a")).thenReturn(mockInnerElement);
        when(mockProductElement.select(".productInfo h3 a").attr("href")).thenReturn("http://localhost");
        when(mockInnerElement.text()).thenReturn("test product");
        when(mockProductElement.select(".pricePerUnit")).thenReturn(mockPriceElements);
        when(mockPriceElements.text()).thenReturn("&pound3.5/unit");

        Elements mockProducts = new Elements(mockProductElement);

        ArrayList<Grocery> arrayList = groceriesApiService.buildGroceriesList(mockProducts);

        verify(mockConnection.execute());

        Grocery grocery = arrayList.get(0);
        assertEquals("test product", grocery.getTitle());
        assertEquals("0kb", grocery.getSize());
        assertEquals(3.5, grocery.getUnitPrice(), 0);
        assertEquals("test desc", grocery.getDescription());
    }
}