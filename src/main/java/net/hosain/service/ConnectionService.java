package net.hosain.service;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

@Service
public class ConnectionService {

    public Connection getConnection(String url) {
        return Jsoup.connect(url);
    }
}
