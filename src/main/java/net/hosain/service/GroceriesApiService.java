package net.hosain.service;

import net.hosain.model.Grocery;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

@Service
public class GroceriesApiService {

    private static final String PRODUCT_SELECTOR = ".product";
    private static final String PRODUCT_TITLE_SELECTOR = ".productInfo h3 a";
    private static final String PRODUCT_PRICE_SELECTOR = ".pricePerUnit";
    public static final String PRODUCT_TEXT_SELECTOR = ".productText";
    public static final String HREF = "href";
    public static final String POUND = "&pound";
    public static final String UNIT = "/unit";
    public static final String KB = "kb";
    public static final int BYTES_IN_KB = 1000;

    private ConnectionService connectionService;

    @Autowired
    public GroceriesApiService(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public Elements getProducts(Document document) {
        return document.select(PRODUCT_SELECTOR);
    }

    public ArrayList<Grocery> buildGroceriesList(Elements products) throws IOException {
        ArrayList<Grocery> groceries = new ArrayList<>(products.size());
        for (Element product : products) {
            Connection.Response productPageResponse = fetchProductPage(product);

            String title = product.select(PRODUCT_TITLE_SELECTOR).text();
            String size = calculatePageSizeInKb(productPageResponse) + KB;
            double price = parsePrice(product);
            String description = getProductDescription(productPageResponse.parse());

            Grocery grocery = new Grocery(title, size, price, description);
            groceries.add(grocery);
        }
        return groceries;
    }

    private Connection.Response fetchProductPage(Element product) throws IOException {
        Connection connection = connectionService.getConnection(product.select(PRODUCT_TITLE_SELECTOR).attr(HREF));
        return connection.execute();
    }

    private int calculatePageSizeInKb(Connection.Response productPageResponse) {
        return productPageResponse.bodyAsBytes().length / BYTES_IN_KB;
    }

    private double parsePrice(Element product) {
        StringBuilder priceText = new StringBuilder(product.select(PRODUCT_PRICE_SELECTOR).text());
        deleteFromStringBuilder(priceText, POUND);
        deleteFromStringBuilder(priceText, UNIT);
        return Double.parseDouble(priceText.toString());
    }

    private void deleteFromStringBuilder(StringBuilder stringBuilder, String string) {
        int start = stringBuilder.indexOf(string);
        if (start >= 0) {
            stringBuilder.delete(start, start + string.length());
        }
    }

    private String getProductDescription(Document document) {
        return document.select(PRODUCT_TEXT_SELECTOR).first().text();
    }
}
