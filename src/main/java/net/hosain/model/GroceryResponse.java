package net.hosain.model;


import java.util.List;

public class GroceryResponse {
    private List<Grocery> results;
    private double total;

    public GroceryResponse(List<Grocery> results, double total) {
        this.results = results;
        this.total = total;
    }

    public List<Grocery> getResults() {
        return results;
    }

    public double getTotal() {
        return total;
    }
}
