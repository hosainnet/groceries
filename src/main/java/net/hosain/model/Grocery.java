package net.hosain.model;

public class Grocery {

    private String title;
    private String size;
    private double unitPrice;
    private String description;

    public Grocery(String title, String size, double unitPrice, String description) {
        this.title = title;
        this.size = size;
        this.unitPrice = unitPrice;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getSize() {
        return size;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public String getDescription() {
        return description;
    }
}
