package net.hosain.controller;

import net.hosain.model.Grocery;
import net.hosain.model.GroceryResponse;
import net.hosain.service.ConnectionService;
import net.hosain.service.GroceriesApiService;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;

@RestController
public class GroceriesApiController {

    private static final String URL = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html";
    private final GroceriesApiService groceriesApiService;
    private ConnectionService connectionService;

    @Autowired
    public GroceriesApiController(GroceriesApiService groceriesApiService, ConnectionService connectionService) {
        this.groceriesApiService = groceriesApiService;
        this.connectionService = connectionService;
    }

    @RequestMapping("/api/groceries/list")
    public GroceryResponse list() throws IOException {
        Document document = connectionService.getConnection(URL).get();
        Elements products = groceriesApiService.getProducts(document);
        ArrayList<Grocery> groceries = groceriesApiService.buildGroceriesList(products);
        return new GroceryResponse(groceries, calculateUnitPrices(groceries));
    }

    private double calculateUnitPrices(ArrayList<Grocery> groceries) {
        //This could be calculated on the fly in the service for better performance
        double totalPrices = 0;
        for (Grocery grocery : groceries) {
            totalPrices += grocery.getUnitPrice();
        }
        return totalPrices;
    }
}