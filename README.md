The solution is implemented using Spring Boot/Java 8.

To start the app, run `./gradlew bootRun` in the project's root folder.

The JSON endpoint is available at http://localhost:8080/api/groceries/list

Run the unit tests using `./gradlew test`


 
 